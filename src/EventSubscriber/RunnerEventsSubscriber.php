<?php
namespace Druman\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Druman\Event\ManagerExecutedInAliasEvent;

class RunnerEventsSubscriber implements EventSubscriberInterface
{
  public static function getSubscribedEvents(): array {
    return [
      ManagerExecutedInAliasEvent::class => 'onManagerExecutedInAlias',
    ];
  }

  public function onManagerExecutedInAlias(ManagerExecutedInAliasEvent $event) {
  }
}
