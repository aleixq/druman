<?php
namespace Druman\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Druman\Event\ManagerExecutedInAliasEvent;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Mime\DraftEmail;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;


class MailerSubscriber implements EventSubscriberInterface
{
  public static function getSubscribedEvents(): array {
    return [
      ManagerExecutedInAliasEvent::class => 'onManagerExecutedInAlias',
    ];
  }

  public function onManagerExecutedInAlias(ManagerExecutedInAliasEvent $event) {
    $filesystem = new Filesystem();
    // Will prepare the mails in standard /tmp directory
    $alias = $event->getAlias();
    $mail_draft_dest = "/tmp/" . filter_var($alias['alias']) . ".eml";
    $converter = new AnsiToHtmlConverter();
    $html = $converter->convert($event->getMailText());
    //$html = preg_replace('/(<\/span>)/i', '$1<br/>
//', $html);
       $message = (new DraftEmail())
        ->from('info@communia.org')
        ->subject("Druman maintenance update for {$alias['alias']}")
        ->html("<!DOCTYPE html><html><body><h1>Druman maintenance update</h1>Upgrade progress:<br/><pre>{$html}</pre></body></html>\n");
//        ->html(fopen($mail_draft_dest . ".out", 'r'))
      ;
        $filesystem->dumpFile($mail_draft_dest, $message->toString());
   
  }
}
