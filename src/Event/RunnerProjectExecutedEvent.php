<?php
namespace Druman\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * This event is dispatched each time a runner project
 * command is executed.
 */
final class RunnerProjectExecutedEvent extends Event
{
    public function __construct(private Array $alias, $mail_text, $new_text) {}

    public function getAlias(): Array
    {
        return $this->alias;
    }

    public function getMailText(): Array
    {
        return $this->mail_text;
    }
    public function getNewText(): Array
    {
        return $this->new_text;
    }
}
