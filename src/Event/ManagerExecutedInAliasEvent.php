<?php
namespace Druman\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * This event is dispatched each time a runner project
 * command is executed foreach alias in a manager.
 */
final class ManagerExecutedInAliasEvent extends Event
{
    public function __construct(private Array $alias, private string $mail_text) {}

    public function getAlias(): Array
    {
        return $this->alias;
    }

    public function getMailText(): string
    {
        return $this->mail_text;
    }
}
