<?php
// src/Command/ManagerRunnerProjectsCommand.php
namespace Druman\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Druman\Event\ManagerExecutedInAliasEvent;


class ManagerRunnerProjectsCommand extends RunnerProjectsCommand
{
  protected function configure()
  {
    // ...
    $this
      // the name of the command (the part after "bin/console")
      ->setName('projects:update')

      // the short description shown while running "php bin/console list"
      ->setDescription('Run manager update command on each project.')

      // the full command description shown when running the command with
      // the "--help" option
      ->setHelp('This command allows you to run the manager update command on each projects that can be managed...')
      ->setDefinition(
        new InputDefinition([
          new InputOption('group', 'g', InputOption::VALUE_OPTIONAL, 'Run only on these projects which are members of specified group'),
          new InputOption('alias', 'a', InputOption::VALUE_OPTIONAL, 'Run only on this specific alias'),
	  new InputOption('all', 'all', InputOption::VALUE_NONE, 'Run in all alias, excluding those using drush8-alias manager'),
	  new InputOption('local', 'l', InputOption::VALUE_NONE, 'List only local projects'),
	  new InputOption('remote', 'r', InputOption::VALUE_NONE, 'List only remote projects'),
	  new InputOption('mail', 'm', InputOption::VALUE_NONE, 'Prepares a mail foreach command ran in project'),
      ]));
  }

  protected function initialize(InputInterface $input, OutputInterface $output){
    $this->order = "null";
    $this->prepareMail = $input->getOption('mail');
  }

  protected function executeInAlias(InputInterface $input, OutputInterface $output, $alias)
  {
    $result = 0;
    $php = PHP_BINARY;
    if ($alias['manager'] == 'test') {
      $this->order = "composer outdated";
      $result += parent::executeInAlias($input, $output, $alias);
    } 
    if ($alias['manager'] == 'drupal-composer' ){
      //  Process for Drupals with drupal-composer.
      $composer = $this->whichComposer($output);

      // Set to maintenance mode:
      $this->order = "${php} $composer exec -v -- drush sset system.maintenance_mode 1";
      $result += parent::executeInAlias($input, $output, $alias);
      
      // chmod setting folder properly:
      $this->order = "chmod 755 web/sites/default";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "find web/sites/default -name \"*settings.php\" -exec chmod 755 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "find web/sites/default -name \"*services.yml\" -exec chmod 755 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);

      //first update core and scaffold
      $this->order = "${php} $composer update drupal/core-recommended  drupal/core-composer-scaffold --with-dependencies";
      $result += parent::executeInAlias($input, $output, $alias);
      //then the modules
      $this->order = "${php} $composer update drupal/* --with-dependencies";
      $result += parent::executeInAlias($input, $output, $alias);
      // Strict chmod to default firectory settings:
      $this->order = "find web/sites/default -name \"*settings.php\" -exec chmod 555 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "find web/sites/default -name \"*services.yml\" -exec chmod 555 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "chmod 555 web/sites/default";
      $result += parent::executeInAlias($input, $output, $alias);

      //update db:
      $this->order = "${php} $composer exec drush updatedb -v";
      $result += parent::executeInAlias($input, $output, $alias);
      //rebuild cache
      $this->order = "${php} $composer exec drush cr -v";
      $result += parent::executeInAlias($input, $output, $alias);
      // Set off maintenance mode:
      $this->order = "${php} $composer exec -v -- drush sset system.maintenance_mode 0";
      $result += parent::executeInAlias($input, $output, $alias);
    }
    if ($alias['manager'] == 'drupal-remote-composer' ){
      //  Process for Drupals with drupal-composer.
      //$composer = $this->whichComposer($output);
      // Hardcode composer and php remote by now #TODO
      $composer = 'composer';
      $php = 'php';

      // Set to maintenance mode:
      $this->order = "${php} $composer exec -v -- drush sset system.maintenance_mode 1";
      $result += parent::executeInAlias($input, $output, $alias);
      
      // chmod setting folder properly:
      $this->order = "chmod 755 web/sites/default";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "find web/sites/default -name \"*settings.php\" -exec chmod 755 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "find web/sites/default -name \"*services.yml\" -exec chmod 755 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);

      //first update core and scaffold
      $this->order = "${php} $composer update drupal/core-recommended  drupal/core-composer-scaffold --with-dependencies";
      $result += parent::executeInAlias($input, $output, $alias);
      //then the modules
      $this->order = "${php} $composer update drupal/* --with-dependencies";
      $result += parent::executeInAlias($input, $output, $alias);
      // Strict chmod to default firectory settings:
      $this->order = "find web/sites/default -name \"*settings.php\" -exec chmod 555 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "find web/sites/default -name \"*services.yml\" -exec chmod 555 {} \;";
      $result += parent::executeInAlias($input, $output, $alias);
      $this->order = "chmod 555 web/sites/default";
      $result += parent::executeInAlias($input, $output, $alias);

      //update db:
      $this->order = "${php} $composer exec drush updatedb -v";
      $result += parent::executeInAlias($input, $output, $alias);
      //rebuild cache
      $this->order = "${php} $composer exec drush cr -v";
      $result += parent::executeInAlias($input, $output, $alias);
      // Set off maintenance mode:
      $this->order = "${php} $composer exec -v -- drush sset system.maintenance_mode 0";
      $result += parent::executeInAlias($input, $output, $alias);
    }
    if ($alias['manager'] == 'drush8' or $alias['manager'] == 'drush8-alias'){
      // Process for Drupals 7 and drush <=8;
      if ($alias['manager'] == "drush8") {
        $drush = $this->whichDrush($output, "drush");
        $drush = "${php} $drush";
      }
      if ($alias['manager'] == "drush8-alias") {
        // Will treat as drush command so no need to prepend drush here
        $drush ="";
        $output->writeln(sprintf('<info>DRUSH alias!</info>'));
      }

      $drupal_version = $this->getDrupalVersion($output, $alias['path']);
      $drupal_major_version = explode(".", $drupal_version, 2)[0];
      $output->writeln(sprintf('<info>Drupal version: %s</info>', $drupal_major_version));
      
      // Set to maintenance mode:
      $set_maintenance_mode = ($drupal_major_version == 8 )? "sset system.maintenance_mode":"vset maintenance_mode" ;
      $this->order = "$drush $set_maintenance_mode 1";
      $result += parent::executeInAlias($input, $output, $alias);
      //first update all
      $this->order = "$drush up";
      $result += parent::executeInAlias($input, $output, $alias);
      //update db:
      $this->order = "$drush updatedb";
      $result += parent::executeInAlias($input, $output, $alias);
      //rebuild cache
      $cache_clear = ($drupal_major_version == 8 )? "cr" : "cc";
      $this->order = "$drush $cache_clear all";
      $result += parent::executeInAlias($input, $output, $alias);
      //run cron:
      $this->order = "$drush  cron";
      $result += parent::executeInAlias($input, $output, $alias);
      // Set off maintenance mode:
      $this->order = "$drush $set_maintenance_mode 0";
      $result += parent::executeInAlias($input, $output, $alias);
    }
    $executed_in_alias_event = new ManagerExecutedInAliasEvent($alias, $this->mailBody);
    $this->dispatcher->dispatch($executed_in_alias_event);

    return $result;
  }

  protected function getDrupalVersion(OutputInterface $output, $path){
    $process = Process::fromShellCommandline(sprintf('cd %s && drush status', $path));
    $process->start();
    $version = '';

    foreach ($process as $type => $data) {
      if ($process::OUT === $type) {
        if (preg_match('|Drupal version +: +(.*)|', $data, $matches)) {
          $version = trim($matches[1]);
        }
      } else {
        echo "[ERR] ".$data."-----data\n";
      }
    }
    return $version;

  }

  protected function whichComposer(OutputInterface $output){
    $process = Process::fromShellCommandline('which composer');
    $composer_path = '';
    try {
        $process->mustRun();
        $composer_path = trim($process->getOutput());
    } catch (ProcessFailedException $exception) {
	echo $exception->getMessage();
        echo "[ERR] No composer found" . $exception->getMessage() . "\n";
    }
    return $composer_path;
  }

  protected function whichDrush(OutputInterface $output, $drush){
    $process = Process::fromShellCommandline("which $drush");
    $drush_path = '';
    try {
        $process->mustRun();
        $drush_path = trim($process->getOutput());
    } catch (ProcessFailedException $exception) {
	echo $exception->getMessage();
        echo "[ERR] No drush found" . $exception->getMessage() . "\n";
    }
    return $drush_path;
  }
}
